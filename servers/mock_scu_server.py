# mocks the SCU api to a degree as needed for developing

import sys
import json
import atexit
import logging

import threading
import flask
from flask import request

import time


import sys
sys.path.insert(0, r"D:\repos\sculib\src")

from mke_sculib.scu import scu
from mke_sculib.mock_telescope import Telescope


_log = logging.getLogger("mke_clientlib.mock_scu")


class TelescopeThread(threading.Thread, Telescope):
    def __init__(self, speedup_factor=1.0, UPDATE_INTERVAL=1.0 ) -> None:
        threading.Thread.__init__(self)
        Telescope.__init__(self, speedup_factor=speedup_factor, use_realtime=True, UPDATE_INTERVAL=UPDATE_INTERVAL)

    def run(self):
        while not self.stop:
            t_last = time.time()
            self.update()
            dt_elapsed = time.time() - t_last
            dt_sleep = self.UPDATE_INTERVAL - dt_elapsed
            if dt_sleep > 0:
                time.sleep(dt_sleep)



def create_app():
    """
    create flask app
    """
    app = flask.Flask(__name__)
    global _log
    _log = app.logger
    telescope = add_telescope_sim(app)
    app.config["DEBUG"] = True

    return app


def add_telescope_sim(app):

    global telescope
    telescope = TelescopeThread()
    telescope.start()

    def interrupt():
        global telescope
        telescope.stop = True

    atexit.register(interrupt)


    def flask_wrap(telescope_command):
        return telescope_command['body'], telescope_command['status']

    app.route('/devices/command', methods=['PUT'], endpoint='devices_command')(lambda: flask_wrap(telescope.devices_command(request.json)))
    app.route('/devices/statusValue', methods=['GET'], endpoint='devices_statusValue')(lambda: flask_wrap(telescope.devices_statusValue(request.args)))
    app.route('/datalogging/currentState', methods=['GET'], endpoint='datalogging_currentState')(lambda: flask_wrap(telescope.datalogging_currentState()))
    app.route('/datalogging/lastSession', methods=['GET'], endpoint='datalogging_lastSession')(lambda: flask_wrap(telescope.datalogging_lastSession()))
    app.route('/datalogging/start', methods=['PUT'], endpoint='datalogging_start')(lambda: flask_wrap(telescope.datalogging_start()))
    app.route('/datalogging/stop', methods=['PUT'], endpoint='datalogging_stop')(lambda: flask_wrap(telescope.datalogging_stop()))

    # app.route('/acu.dish_management_controller.move_to_band', methods=['PUT'], endpoint='datalogging_stop')(lambda: flask_wrap(telescope.datalogging_stop()))

    app.route("/acuska/programTrack", methods=['PUT'], endpoint='program_track')(lambda: flask_wrap(telescope.program_track(request.data)))

    app.route('/datalogging/exportSession', methods=['GET'], endpoint='datalogging_exportSession')(lambda: flask_wrap(telescope.datalogging_exportSession(request.args)))
    app.route('/datalogging/sessions', methods=['GET'], endpoint='datalogging_sessions')(lambda: flask_wrap(telescope.datalogging_sessions(request.args)))

    @app.route('/', defaults={'u_path': ''}, methods=['GET', 'PUT'])
    @app.route('/<path:u_path>', methods=['GET', 'PUT'])
    def catch_all(u_path):
        app.logger.debug("Received request: {}".format(request))   # pylint: disable=no-member

        with telescope.dataLock:
            msg = json.dumps(telescope.data)
        return (msg, 200)

    return telescope


if __name__ == "__main__":
    app = create_app()
    app.run(host="0.0.0.0", port=8080, debug=True, use_reloader=False)
