import datetime

import pandas as pd

import collections

import dash
from dash import dcc, html
from plotly.subplots import make_subplots
import plotly.graph_objects as go
from dash import Dash, dash_table

from dash.dependencies import Input, Output

from astropy.time import Time




import os, inspect, sys
# path was needed for local testing
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
print(parent_dir)
sys.path.insert(0, parent_dir + '/src')
print(sys.path)





##### 
# PARAMS
######

my_ip = '0.0.0.0'
my_port = 8018
debug = False
do_add_telescope_sim = True
n_points_plot_max = 360
t_update_sec = 1.0

# not used when add_telescope_sim == True
telescope_ip = 'localhost'
telescope_port = str(my_port)



title = "MKE Telescope Monitor: http://" + telescope_ip + ':' + telescope_port
columns = "acu.time.internal_time;acu.azimuth.p_act;acu.azimuth.p_set;acu.elevation.p_act;acu.elevation.p_set;acu.feed_indexer.p_act;acu.feed_indexer.p_set;acu.general_management_and_controller.state;acu.stow_pin_controller.azimuth_status".split(';')


data = {}


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.title = title

app.layout = html.Div(
    html.Div([
        html.H4(title),
        html.Div(id='live-update-text'),
        dcc.Graph(id='live-update-graph-az'),
        dcc.Graph(id='live-update-graph-el'),
        dcc.Graph(id='live-update-graph-fi'),

        dcc.Interval(
            id='interval-component',
            interval=int(t_update_sec*1000), # in milliseconds
            n_intervals=0
        )
    ])
)



if do_add_telescope_sim:

    import mock_scu_server as mock_scu
    telescope = mock_scu.add_telescope_sim(app.server)

    def tick_get_data():
        data = telescope.get_many(columns)
        return data

else:  

    from mke_sculib.scu import scu
    mpi = scu(ip=telescope_ip, port=telescope_port, debug=debug)

    def tick_get_data():
        data = mpi.get_device_status_value(columns)
        return dict(zip(columns, data))






# Multiple components can update everytime interval gets fired.
@app.callback(Output('live-update-graph-az', 'figure'),
              Output('live-update-graph-el', 'figure'),
              Output('live-update-graph-fi', 'figure'),
              Output('live-update-text', 'children'),
              Input('interval-component', 'n_intervals'))
def update_graph_live(n):

    row = tick_get_data()
    row['time'] = Time(row['acu.time.internal_time'], format='mjd').datetime
    global data

    for k in row.keys():
        if k not in data:
            data[k] = []
        if len(data[k]) > n_points_plot_max:
            data[k].pop(0)
        data[k].append(row[k])

    

    # Create the graph with subplots
    fig = make_subplots(rows=1, cols=1, vertical_spacing=0.2)
    fig['layout']['margin'] = {
        'l': 30, 'r': 10, 'b': 30, 't': 10
    }
    fig['layout']['legend'] = {'x': 0, 'y': 1, 'xanchor': 'left'}

    fig.append_trace({
        'x': data['time'],
        'y': data['acu.azimuth.p_act'],
        'name': 'acu.azimuth.p_act',
        'mode': 'lines+markers',
        'type': 'scatter'
    }, 1, 1)
    fig.append_trace({
        'x': data['time'],
        'y': data['acu.azimuth.p_set'],
        'name': 'acu.azimuth.p_set',
        'mode': 'lines+markers',
        'type': 'scatter'
    }, 1, 1)
    fig1 = fig

    to_plot = [(f'acu.{s}.p_act', f'acu.{s}.p_set') for s in ['azimuth', 'elevation', 'feed_indexer']]

    def mkplot(data, k1, k2):

        fig = make_subplots(rows=1, cols=1, vertical_spacing=0.2 )
        fig['layout']['margin'] = {
            'l': 30, 'r': 10, 'b': 30, 't': 10
        }
        fig['layout']['height'] = 300
        fig['layout']['legend'] = {'x': 0, 'y': 1, 'xanchor': 'left'}

        for k in [k1, k2]:
            fig.append_trace({
                'x': data['time'],
                'y': data[k],
                'name': k,
                'mode': 'lines+markers',
                'type': 'scatter'
            }, 1, 1)
            
        return fig

    (fig1, fig2, fig3) = [mkplot(data, k1, k2) for (k1, k2) in to_plot]

    cc=[{'id': c, 'name': c} for c in ['time'] + columns]
    tbl = dash_table.DataTable(data=[row], columns=cc)

    return fig1, fig2, fig3, tbl


if __name__ == '__main__':
    app.run(debug=debug, port=my_port, host=my_ip)