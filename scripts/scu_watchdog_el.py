

from mke_sculib.scu import scu as scu_api
import datetime



antenna_ip = '10.96.64.10' 
el_max = 85

scu = scu_api(ip=antenna_ip, debug = False)

def intervene():
    scu.get_command_authority()
    scu.reset_dmc()
    scu.activate_dmc()
    scu.scu_put('/devices/command',
            {'path': 'acu.dish_management_controller.stop'})
    scu.wait_duration(1)
    scu.scu_put('/devices/command',
            {'path': 'acu.dish_management_controller.stop'})
    scu.wait_duration(1)
    scu.interlock_acknowledge_dmc()
    scu.wait_duration(1)
    scu.abs_elevation(el_max-1, 1.0)
    scu.wait_settle(axis='el')
    scu.deactivate_axes()
    scu.wait_duration(5)
    scu.deactivate_dmc()
    scu.wait_duration(5)
    scu.release_command_authority()

el_last = el_max+1
i = 0
while True:
    el_now = scu.get_device_status_value('acu.elevation.p_act')
    t = datetime.utcnow().isoformat()

    if el_now > el_max and el_last < el_max:
        
        print(f'{t} INTERVENING because EL_now = {el_now}')
        intervene()

    
    el_last = el_now
    i += 1
    
    if i % 5_000 == 0:
        print(f'{t} status n_ticks={i}')
    
    scu.wait_duration(0.5)
