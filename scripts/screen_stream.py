import cv2
import numpy as np

from flask import Flask, Response
import traceback

from PIL import Image
import win32gui, win32ui, win32con


def background_screenshot(hwnd):
    rect = win32gui.GetWindowRect(hwnd)
    width = rect[2] - rect[0] + 20
    height = rect[3] - rect[1] + 20

    wDC = win32gui.GetWindowDC(hwnd)
    dcObj=win32ui.CreateDCFromHandle(wDC)
    cDC=dcObj.CreateCompatibleDC()
    dataBitMap = win32ui.CreateBitmap()
    dataBitMap.CreateCompatibleBitmap(dcObj, width, height)
    cDC.SelectObject(dataBitMap)
    cDC.BitBlt((0,0),(width, height) , dcObj, (0,0), win32con.SRCCOPY)
    
    # https://stackoverflow.com/questions/6951557/pil-and-bitmap-from-winapi
    bmpinfo = dataBitMap.GetInfo()
    bmpstr = dataBitMap.GetBitmapBits(True)
    im = Image.frombuffer('RGBA', (bmpinfo['bmWidth'], bmpinfo['bmHeight']), bmpstr, 'raw', 'BGRA', 0, 1)

    dcObj.DeleteDC()
    cDC.DeleteDC()
    win32gui.ReleaseDC(hwnd, wDC)
    win32gui.DeleteObject(dataBitMap.GetHandle())
    return np.asarray(im) # Convet to NumPy array


toplist, winlist = [], []
def enum_cb(hwnd, results):
    winlist.append((hwnd, win32gui.GetWindowText(hwnd)))
win32gui.EnumWindows(enum_cb, toplist)

hwnd, title = [(hwnd, title) for hwnd, title in winlist if 'stellarium' in title.lower()][0]
print(hwnd, title)

# SCREEN_SIZE = tuple(pyautogui.size())
# print(SCREEN_SIZE)
# IMG_SIZE = np.array(SCREEN_SIZE, dtype=int) // 3

IMG_SIZE = None
print(IMG_SIZE)


def gen_frames():

    while True:
        try:

            img = background_screenshot(hwnd)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            ret, buffer = cv2.imencode('.jpg', img)
            frame = buffer.tobytes()

            yield (b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one and show result
        
        except Exception as err:
            print('Image plotting error!' + str(err))
            print(traceback.format_exc())



app = Flask("Stellarium Web Stream")

@app.route('/')
@app.route('/index')
@app.route('/vid')
def vid():
     return Response(gen_frames(),mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='localhost',port=5000, debug=False, threaded=True)
