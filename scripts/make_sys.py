from typing import Any
import numpy as np
import matplotlib.pyplot as plt
import control as ct
from control.matlab import *


display = print

d = 0.2
# d = 1.3
omega_0 = 2.0 * (2*np.pi)


# omega_0 = (1 / T)**0.5
T = 1. / omega_0

Ts = 0.1

num = 1
den = [T**2, 2*d*T, 1]

sys = ct.tf(num, den)
sysd = sys.sample(Ts)

plt.figure()

bode(sys, np.linspace(0.01, 4, 200)*np.pi*2, Hz=True)
plt.axvline(1.8, color='k')

mag, phase, omega = bode(sysd, np.linspace(0.01, 4, 200)*np.pi*2, Hz=True)

plt.figure()
plt.plot(omega/np.pi/2, mag)
plt.grid()
plt.axvline(1.8, color='k')

plt.figure()

res, t = step(sys)
plt.plot(t, res)

res, t = step(sysd)
plt.plot(t, res)

d_el = 0.15
d_az = 0.35

num_noise = [0.4]
num_az = [1]
num_el = [1]

den_el = [T**2, 2*d_el*T, 1]
den_az = [T**2, 2*d_az*T, 1]

ss_sys = ss(sysd)

display(ss_sys.A)
display(ss_sys.B)
display(ss_sys.C)
display(ss_sys.D)


z = [0]
one = [1]

g_az2az = ss(tf(num_az, den_az).sample(Ts))
g_el2el = ss(tf(num_el, den_el).sample(Ts))
g_n2el = ss(tf(num_noise, den_el).sample(Ts))
g_n2az = ss(tf(num_noise, den_az).sample(Ts))

def disp_ss(g, name):
    print('='*60)
    print(name)
    display(g)
    print('A')
    display(ss_sys.A)
    print('B')
    display(ss_sys.B)
    print('C')
    display(ss_sys.C)
    print('D')
    display(ss_sys.D)

disp_ss(g_az2az, 'g_az2az')
disp_ss(g_el2el, 'g_el2el')
disp_ss(g_n2az, 'g_n2az')
disp_ss(g_n2el, 'g_el2el')




#%% 

def combine(m1, m2):
    A01 = np.zeros_like(m1)
    A10 = np.zeros_like(m2)

    A0 = np.concatenate((m1, A01), axis=1)
    A1 = np.concatenate((A10, m2.A), axis=1)

    A = np.concatenate((A0, A1), axis=0)
    return A



A = combine(g_az2az.A, g_el2el.A)
C = combine(g_az2az.C, g_el2el.C)

B0 = combine(g_az2az.B, g_el2el.B)
B1 = [[0.4], [0], [0.4], [0]]
B = np.concatenate((B0, B1), axis=1)

# no direct feedthrough
D = None


class SS:
    def __init__(self, A, B, C, D=None) -> None:
        self.A = A
        self.B = B
        self.C = C
        self.D = D if D is not None else np.zeros((C.shape[0], B.shape[1]))
        self.x = np.zeros((A.shape[0],))[:, None]

    def __call__(self, u) -> Any:
        
        u = u[None, :] if len(u.shape) == 1 else u
        
        A, B, C, D = self.A, self.B, self.C, self.D
        x = self.x

        y = C @ x + D @ u
        x = A @ x + B @ u

        self.x = x
        return y