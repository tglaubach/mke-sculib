#%% add the path where the 

import os, inspect, sys
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
if __name__ == '__main__':
    print(parent_dir)
    sys.path.insert(0, parent_dir)

from mke_sculib.scu import scu



#%%
import mke_sculib

print(mke_sculib.__version__)
print(mke_sculib.__file__)



#%%%

#mpi = scu('http://134.104.22.44:8080/')
mpi = mke_sculib.load('119A')

# %%
print(mpi.version_acu)

channels = mpi.get_channel_list(with_values=True, with_timestamps=True)

for k, t, v in channels:
    if 'work' in k or 'run' in k or 'dist' in k:
        print(k, v, t)

